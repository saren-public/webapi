package com.dinsaren.webapi.services;

import com.dinsaren.webapi.exceptions.WebException;
import com.dinsaren.webapi.models.Product;
import com.dinsaren.webapi.models.ProductUnit;
import com.dinsaren.webapi.models.request.PaginationRequest;
import com.dinsaren.webapi.models.response.ProductRequest;

import java.util.List;

public interface ProductService {
    List<Product> getAllProducts();
    void create(Product req) throws WebException;
    Product getProductById(Integer id);
    void update(Product req) throws WebException;
    void delete(Product req) throws WebException;
    List<Product> getAllProductPagination(PaginationRequest req) throws WebException;
    List<ProductUnit> getAllProductUnit(Integer productId) throws WebException;
    void createProductUnit(ProductUnit req) throws WebException;
    void updateProductUnit(ProductUnit req) throws WebException;
    void deleteProductUnit(ProductUnit req) throws WebException;
    Product searchProduct(ProductRequest req) throws WebException;
}
