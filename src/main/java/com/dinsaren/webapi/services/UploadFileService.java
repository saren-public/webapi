package com.dinsaren.webapi.services;

import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface UploadFileService {
    String uploadFile(MultipartFile files);
}
