package com.dinsaren.webapi.services;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

@Slf4j
@Service
public class UploadFileServiceImpl implements UploadFileService {

    @Value("${spring.upload.server.path}")
    String serverPath;

    @Override
    public String uploadFile(MultipartFile files) {

        if (files.isEmpty()) {
            log.error("Failed to store empty file");
        }

        String writePath = serverPath;

        File file = new File(writePath);
        if (!file.exists()) {
            file.mkdirs();

        }
        String pathImage = "";
        String name = StringUtils.cleanPath(files.getOriginalFilename());
        String extension = getFileExtension(name);
        String extensions = "jpeg,png,jpg";
        if (!extensions.contains(extension.toLowerCase())) {
            log.error("Invalid File Extension {}", extension);
        }
        String filePath = Calendar.getInstance().getTimeInMillis() + "." + extension;
        file = new File(writePath + filePath);
        try {
            files.transferTo(file);
            pathImage = file.getPath();
            if (!files.isEmpty()) {

            }
        } catch (IOException e) {
            log.error("upload file fail", e);
        }finally {
            log.info("Final exception {}", filePath);
        }
        return pathImage;
    }

    String getFileExtension(String fileName) {
        int dotIndex = fileName.lastIndexOf('.');
        if (dotIndex < 0) {
            return null;
        }
        return fileName.substring(dotIndex + 1);
    }

    String getFileNoExtension(String fileName) {
        int dotIndex = fileName.lastIndexOf('.');
        if (dotIndex < 0) {
            return null;
        }
        return fileName.substring(0, dotIndex);
    }


}
