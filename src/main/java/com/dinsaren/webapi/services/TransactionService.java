package com.dinsaren.webapi.services;

import com.dinsaren.webapi.exceptions.WebException;
import com.dinsaren.webapi.models.Transaction;

public interface TransactionService {
    void createTransaction(Transaction req) throws WebException;
}
