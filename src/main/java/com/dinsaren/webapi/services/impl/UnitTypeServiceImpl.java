package com.dinsaren.webapi.services.impl;

import com.dinsaren.webapi.constants.Constants;
import com.dinsaren.webapi.constants.ErrorCode;
import com.dinsaren.webapi.exceptions.WebException;
import com.dinsaren.webapi.models.UnitType;
import com.dinsaren.webapi.repositories.UnitTypeRepository;
import com.dinsaren.webapi.services.UnitTypeService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class UnitTypeServiceImpl implements UnitTypeService {
    private final UnitTypeRepository unitTypeRepository;

    @Override
    public List<UnitType> getAllUnitTypes() throws WebException {
        return unitTypeRepository.findAll();
    }

    @Override
    public List<UnitType> getAllUnitTypeStatus(String status)throws WebException {
        return unitTypeRepository.findAllByStatus(status);
    }

    @Override
    public UnitType getUnitTypeById(Integer id)throws WebException {
        return unitTypeRepository.findById(id).orElse(null);
    }

    @Override
    public void create(UnitType req) throws WebException {
        var checkName = unitTypeRepository.findByName(req.getName());
        if(checkName.isPresent()){
            throw new WebException("Unit type have already exist!","Unit type have already exist", ErrorCode.UNIT_TYPE_NAME_ALREADY);
        }
        unitTypeRepository.save(req);
    }

    @Override
    public void delete(Integer id)throws WebException {
        var unit = unitTypeRepository.findById(id);
        if(unit.isEmpty()){
            throw new WebException("Unit type is not found","Unit type is not found","ERR-003");
        }
        unit.get().setStatus(Constants.STATUS_DELETE);
    }

    @Override
    public void update(UnitType req) throws WebException {
        if(getUnitTypeById(req.getId()) == null){
            throw new WebException("Unit type is not found","Unit type is not found","ERR-003");
        }
        unitTypeRepository.save(req);
    }
}
