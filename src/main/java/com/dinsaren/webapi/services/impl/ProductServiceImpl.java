package com.dinsaren.webapi.services.impl;

import com.dinsaren.webapi.constants.Constants;
import com.dinsaren.webapi.exceptions.WebException;
import com.dinsaren.webapi.models.Product;
import com.dinsaren.webapi.models.ProductUnit;
import com.dinsaren.webapi.models.request.PaginationRequest;
import com.dinsaren.webapi.models.response.ProductRequest;
import com.dinsaren.webapi.repositories.ProductRepository;
import com.dinsaren.webapi.repositories.ProductUnitRepository;
import com.dinsaren.webapi.services.CategoryService;
import com.dinsaren.webapi.services.ProductService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
@RequiredArgsConstructor
public class ProductServiceImpl implements ProductService {
    private final ProductRepository productRepository;
    private final CategoryService categoryService;
    private final ProductUnitRepository productUnitRepository;
    @Override
    public List<Product> getAllProducts() {
        return productRepository.findAll();
    }

    @Override
    public void create(Product req) throws WebException {
        var category = categoryService.getCategoryById(req.getCategory().getId());
        if(category == null){
            throw new WebException("Please select categories","Please select categories","500");
        }
        if(null == req.getCode() || "".equals(req.getCode()) || "".equals(req.getName()) || "".equals(req.getBarcode())){
            throw new WebException("Product name, code, barcode is required","Product name, code, barcode is required","500");
        }

        var code = productRepository.findByCode(req.getCode());
        if(code.isPresent()){
            throw new WebException("Product code is already exist","Product code is already exist", "500");
        }

        var barcode = productRepository.findByBarcode(req.getBarcode());
        if(barcode.isPresent()){
            throw new WebException("Product barcode is already exist","Product barcode is already exist", "500");
        }
        req.setCreatedDate(new Date());
        req.setCreatedBy("SYS");
        req.setStatus(Constants.STATUS_ACTIVE);
        productRepository.save(req);
    }

    @Override
    public Product getProductById(Integer id) {
        return productRepository.findById(id).orElse(null);
    }

    @Override
    public void update(Product req) throws WebException {
        var product = productRepository.findById(req.getId());
        if(product.isEmpty()){
            throw new WebException("Product is not found","Product is not found ","500");
        }

        var category = categoryService.getCategoryById(req.getCategory().getId());
        if(category == null){
            throw new WebException("Please select categories","Please select categories","500");
        }
        if(null == req.getCode() || "".equals(req.getCode()) || "".equals(req.getName()) || "".equals(req.getBarcode())){
            throw new WebException("Product name, code, barcode is required","Product name, code, barcode is required","500");
        }

        var code = productRepository.findByCode(req.getCode());
        if(code.isPresent()){
            if(!code.get().getCode().equals(product.get().getCode())) {
                throw new WebException("Product code is already exist", "Product code is already exist", "500");
            }
        }

        var barcode = productRepository.findByBarcode(req.getBarcode());
        if(barcode.isPresent()){
            if(!barcode.get().getBarcode().equals(product.get().getBarcode())) {
                throw new WebException("Product barcode is already exist", "Product barcode is already exist", "500");
            }
        }
        productRepository.save(req);

    }

    @Override
    public void delete(Product req) throws WebException {
        var product = productRepository.findById(req.getId());
        if(product.isPresent()){
            product.get().setStatus(Constants.STATUS_DELETE);
            productRepository.save(product.get());
        }
    }

    @Override
    public List<Product> getAllProductPagination(PaginationRequest req) throws WebException {
        Pageable paging = PageRequest.of(req.getPage() - 1, req.getLimit());
        return  productRepository.findAllByStatusOrderByIdDesc(req.getStatus(), paging).getContent();
    }

    @Override
    public List<ProductUnit> getAllProductUnit(Integer productId) throws WebException {
        return productUnitRepository.findAllByProduct_IdOrderBySaleDefaultDesc(productId);
    }

    @Override
    public void createProductUnit(ProductUnit req) throws WebException {
        var productUintExist = productUnitRepository.findByProduct_IdAndUnitType_Id(req.getProduct().getId(), req.getUnitType().getId());
        if(productUintExist != null){
            throw new WebException("Unit Type is already exist", "Unit Type is already exist", "500");
        }
        // unit id
        // product id
        productUnitRepository.save(req);
    }

    @Override
    public void updateProductUnit(ProductUnit req) throws WebException {
        var productUnit = productUnitRepository.findById(req.getId());
        if(productUnit.isEmpty()){
            throw new WebException("Product unit not found", "Product unit not found", "500");
        }

        var saleDefault = productUnitRepository.findBySaleDefaultAndProductId(Constants.Y,req.getProduct().getId());
        if(saleDefault.getId() != productUnit.get().getId()){
            saleDefault.setSaleDefault(Constants.N);
            productUnitRepository.save(saleDefault);
        }
        productUnitRepository.save(req);
    }

    @Override
    public void deleteProductUnit(ProductUnit req) throws WebException {
        var productUnit = productUnitRepository.findById(req.getId());
        if(productUnit.isEmpty()){
            throw new WebException("Product unit not found", "Product unit not found", "500");
        }
        productUnitRepository.delete(req);
    }

    @Override
    public Product searchProduct(ProductRequest req) throws WebException {
        var product = new Product();
        if(!req.getCode().equals("")){
             product = productRepository.findByCode(req.getCode()).orElse(null);
        }

        if(!req.getBarcode().equals("")){
             product = productRepository.findByBarcode(req.getCode()).orElse(null);
        }
        if(product == null){
            throw new WebException("Product not found", "Product  not found", "500");
        }
        var listProductUnit = productUnitRepository.findAllByProduct_IdOrderBySaleDefaultDesc(product.getId());
        if(!listProductUnit.isEmpty()){
            product.setProductUnits(listProductUnit);
        }
        return product;
    }

}
