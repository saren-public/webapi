package com.dinsaren.webapi.services.impl;

import com.dinsaren.webapi.exceptions.WebException;
import com.dinsaren.webapi.models.Transaction;
import com.dinsaren.webapi.repositories.TransactionItemRepository;
import com.dinsaren.webapi.repositories.TransactionRepository;
import com.dinsaren.webapi.services.TransactionService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
@RequiredArgsConstructor
public class TransactionServiceImpl implements TransactionService {
    private final TransactionRepository transactionRepository;
    private final TransactionItemRepository transactionItemRepository;

    @Override
    public void createTransaction(Transaction req) throws WebException {
        req.setTransactionDate(new Date());
        Transaction transaction = transactionRepository.save(req);
        if(transaction.getId() != 0){
            req.getItems().forEach(item->{
                item.setTransactionId(transaction.getId());
                transactionItemRepository.save(item);
            });
        }
    }
}
