package com.dinsaren.webapi.services.impl;

import com.dinsaren.webapi.exceptions.WebException;
import com.dinsaren.webapi.models.Category;
import com.dinsaren.webapi.repositories.CategoryRepository;
import com.dinsaren.webapi.services.CategoryService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class CategoryServiceImpl implements CategoryService {
    // Call repository
    // Object Injection by field, method, constructor
    private final CategoryRepository categoryRepository;

    @Override
    public List<Category> getAllCategories() {
        return categoryRepository.findAll();
    }

    @Override
    public Category getCategoryById(Integer id) {
        return categoryRepository.findById(id).orElse(null);
    }

    @Override
    public void create(Category category) throws WebException {
        var checkCategoryName = categoryRepository.findByName(category.getName());
        if(checkCategoryName.isPresent()){
            throw new WebException(
                    "Category have already exist",
                    "Category have already exist",
                    "ERR-001");
        }
        category.setId(0);
        category.setStatus("ACT");
        categoryRepository.save(category);
    }

    @Override
    public void update(Category category) {
        if(getCategoryById(category.getId()) != null){
            categoryRepository.save(category);
        }
    }
}
