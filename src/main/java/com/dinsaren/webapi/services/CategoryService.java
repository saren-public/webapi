package com.dinsaren.webapi.services;

import com.dinsaren.webapi.exceptions.WebException;
import com.dinsaren.webapi.models.Category;

import java.util.List;

public interface CategoryService {
    List<Category> getAllCategories();
    Category getCategoryById(Integer id);
    void create(Category category) throws WebException;
    void update(Category category);

}
