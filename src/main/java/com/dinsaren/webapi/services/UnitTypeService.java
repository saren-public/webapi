package com.dinsaren.webapi.services;

import com.dinsaren.webapi.exceptions.WebException;
import com.dinsaren.webapi.models.UnitType;

import java.util.List;

public interface UnitTypeService {
    List<UnitType> getAllUnitTypes() throws WebException;
    List<UnitType> getAllUnitTypeStatus(String status) throws WebException;
    UnitType getUnitTypeById(Integer id) throws WebException;
    void create(UnitType req) throws WebException;
    void delete(Integer id) throws WebException;
    void update(UnitType req) throws WebException;
}
