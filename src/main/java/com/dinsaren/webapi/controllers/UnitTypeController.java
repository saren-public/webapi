package com.dinsaren.webapi.controllers;

import com.dinsaren.webapi.exceptions.WebException;
import com.dinsaren.webapi.models.Category;
import com.dinsaren.webapi.models.UnitType;
import com.dinsaren.webapi.models.response.BaseResponse;
import com.dinsaren.webapi.services.UnitTypeService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@Slf4j
public class UnitTypeController {
    private final UnitTypeService unitTypeService;
    private BaseResponse baseResponse;

    @GetMapping("/api/unitTypes")
    public ResponseEntity<Object> getAll(){
        baseResponse = new BaseResponse();
        try{
            log.info("Intercept get all unit types");
            baseResponse.setGetDataSuccess(unitTypeService.getAllUnitTypes());
            return new ResponseEntity<>(baseResponse, HttpStatus.OK);
        }catch (WebException e){
            log.error("While get error get all unit types {}",e.getMessage());
            baseResponse.setMessage(e.getMessage());
            baseResponse.setCode(e.getCode());
            baseResponse.setMessageKh(e.getMessageKh());
            return new ResponseEntity<>(baseResponse, HttpStatus.OK);
        }
        catch (Throwable e){
            log.error("General error get all unit types",e);
            return new ResponseEntity<>(baseResponse, HttpStatus.OK);
        }finally {
            log.info("Get all unit types response {}", baseResponse);
        }
    }

    @GetMapping("/api/unitTypes/{id}")
    public ResponseEntity<Object> getById(@PathVariable("id") Integer id){
        baseResponse = new BaseResponse();
        try{
            log.info("Intercept get unit types by id {}", id);
            baseResponse.setGetDataSuccess(unitTypeService.getUnitTypeById(id));
            return new ResponseEntity<>(baseResponse, HttpStatus.OK);
        }catch (WebException e){
            log.error("While get error get unit types by id {}",e.getMessage());
            baseResponse.setMessage(e.getMessage());
            baseResponse.setCode(e.getCode());
            baseResponse.setMessageKh(e.getMessageKh());
            return new ResponseEntity<>(baseResponse, HttpStatus.OK);
        }
        catch (Throwable e){
            log.error("General error get unit types by id",e);
            return new ResponseEntity<>(baseResponse, HttpStatus.OK);
        }finally {
            log.info("Get unit types by id {} response {}", id, baseResponse);
        }
    }

    @PostMapping("/api/unitTypes/create")
    public ResponseEntity<Object> create(@RequestBody UnitType req){
        baseResponse = new BaseResponse();
        try{
            log.info("Intercept create unit types req {}", req);
            unitTypeService.create(req);
            baseResponse.setCreateDataSuccess(null);
            return new ResponseEntity<>(baseResponse, HttpStatus.OK);
        }catch (WebException e){
            log.error("While get error get unit types by id {}",e.getMessage());
            baseResponse.setMessage(e.getMessage());
            baseResponse.setCode(e.getCode());
            baseResponse.setMessageKh(e.getMessageKh());
            return new ResponseEntity<>(baseResponse, HttpStatus.OK);
        }
        catch (Throwable e){
            log.error("General error get unit types by id",e);
            return new ResponseEntity<>(baseResponse, HttpStatus.OK);
        }finally {
            log.info("Get unit types by id {} response {}", req, baseResponse);
        }
    }

    @PostMapping("/api/unitTypes/update")
    public ResponseEntity<Object> update(@RequestBody UnitType req){
        baseResponse = new BaseResponse();
        try{
            log.info("Intercept update unit types req {}", req);
            unitTypeService.update(req);
            baseResponse.setUpdateDataSuccess(null);
            return new ResponseEntity<>(baseResponse, HttpStatus.OK);
        }catch (WebException e){
            log.error("While get error get unit types by id {}",e.getMessage());
            baseResponse.setMessage(e.getMessage());
            baseResponse.setCode(e.getCode());
            baseResponse.setMessageKh(e.getMessageKh());
            return new ResponseEntity<>(baseResponse, HttpStatus.OK);
        }
        catch (Throwable e){
            log.error("General error get unit types by id",e);
            return new ResponseEntity<>(baseResponse, HttpStatus.OK);
        }finally {
            log.info("Get unit types by id {} response {}", req, baseResponse);
        }
    }

    @PostMapping("/api/unitTypes/delete")
    public ResponseEntity<Object> delete(@RequestBody UnitType req){
        baseResponse = new BaseResponse();
        try{
            log.info("Intercept delete unit types req {}", req);
            unitTypeService.delete(req.getId());
            baseResponse.setDeleteDataSuccess(null);
            return new ResponseEntity<>(baseResponse, HttpStatus.OK);
        }catch (WebException e){
            log.error("While get error get unit types by id {}",e.getMessage());
            baseResponse.setMessage(e.getMessage());
            baseResponse.setCode(e.getCode());
            baseResponse.setMessageKh(e.getMessageKh());
            return new ResponseEntity<>(baseResponse, HttpStatus.OK);
        }
        catch (Throwable e){
            log.error("General error get unit types by id",e);
            return new ResponseEntity<>(baseResponse, HttpStatus.OK);
        }finally {
            log.info("Get unit types by id {} response {}", req, baseResponse);
        }
    }

}
