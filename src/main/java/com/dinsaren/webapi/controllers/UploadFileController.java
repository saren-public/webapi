package com.dinsaren.webapi.controllers;

import com.dinsaren.webapi.models.response.BaseResponse;
import com.dinsaren.webapi.services.UploadFileService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@Slf4j
@RestController
@RequiredArgsConstructor
public class UploadFileController {
    private final UploadFileService uploadFileService;
    private BaseResponse baseResponse;

    @PostMapping(value = "/image/upload", consumes = {"multipart/form-data"})
    public ResponseEntity<Object> uploadFile(
            @RequestParam("File") MultipartFile file) {
        log.debug("Intercept upload file req {}", file.toString());
        log.info("Intercept upload image");
        baseResponse = new BaseResponse();
        try {
            String imageUrl = uploadFileService.uploadFile(file);
            baseResponse.setCode("200");
            baseResponse.setMessage("Get upload image success");
            baseResponse.setMessageKh("Get upload image success");
            baseResponse.setData(imageUrl);
            return new ResponseEntity<>(baseResponse, HttpStatus.OK);
        }catch (Throwable e){
            log.info("While get error upload image", e);
            baseResponse.setCode("500");
            baseResponse.setMessage("Get upload image un success");
            baseResponse.setMessageKh("Get upload image un success");
            return new ResponseEntity<>(baseResponse, HttpStatus.OK);
        }
    }

}
