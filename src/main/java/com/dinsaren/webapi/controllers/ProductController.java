package com.dinsaren.webapi.controllers;

import com.dinsaren.webapi.exceptions.WebException;
import com.dinsaren.webapi.models.Product;
import com.dinsaren.webapi.models.ProductUnit;
import com.dinsaren.webapi.models.request.PaginationRequest;
import com.dinsaren.webapi.models.response.BaseResponse;
import com.dinsaren.webapi.models.response.ProductRequest;
import com.dinsaren.webapi.services.ProductService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@Controller
@RequiredArgsConstructor
@Slf4j
public class ProductController {
    private final ProductService productService;
    private BaseResponse baseResponse;

    @GetMapping("/api/products")
    public ResponseEntity<Object> getAll(){
        log.info("Intercept get all categories");
        baseResponse = new BaseResponse();
        try {
            var list = productService.getAllProducts();
            baseResponse.setCode("200");
            baseResponse.setMessage("Get all products success");
            baseResponse.setMessageKh("Get all products success");
            baseResponse.setData(list);
            return new ResponseEntity<>(baseResponse, HttpStatus.OK);
        }catch (Throwable e){
            log.info("While get error get all products ", e);
            baseResponse.setCode("500");
            baseResponse.setMessage("Get all products un success");
            baseResponse.setMessageKh("Get all products un success");
            return new ResponseEntity<>(baseResponse, HttpStatus.OK);
        }
    }

    @GetMapping("/api/products/pagination")
    public ResponseEntity<Object> getAllProductPagination(@RequestBody PaginationRequest req){
        log.info("Intercept get all products with pagination");
        baseResponse = new BaseResponse();
        try {
            var list = productService.getAllProductPagination(req);
            baseResponse.setCode("200");
            baseResponse.setMessage("Get all products success");
            baseResponse.setMessageKh("Get all products success");
            baseResponse.setData(list);
            return new ResponseEntity<>(baseResponse, HttpStatus.OK);
        }catch (Throwable e){
            log.info("While get error get all products with pagination ", e);
            baseResponse.setCode("500");
            baseResponse.setMessage("Get all products un success");
            baseResponse.setMessageKh("Get all products un success");
            return new ResponseEntity<>(baseResponse, HttpStatus.OK);
        }
    }

    @PostMapping("/api/products/create")
    public ResponseEntity<Object> create(@RequestBody Product req){
        log.info("Intercept create category");
        baseResponse = new BaseResponse();
        try {
            log.info("Intercept create product req {}", req);
            productService.create(req);
            baseResponse.setCode("200");
            baseResponse.setMessage("Create products success");
            baseResponse.setMessageKh("Create products success");
            return new ResponseEntity<>(baseResponse, HttpStatus.OK);
        }catch (WebException e){
            log.error("Error create product on web exception ", e);
            baseResponse.setMessage(e.getMessage());
            baseResponse.setCode(e.getCode());
            baseResponse.setMessageKh(e.getMessageKh());
            return new ResponseEntity<>(baseResponse, HttpStatus.OK);
        }
        catch (Throwable e){
            log.info("While get error create products ", e);
            baseResponse.setCode("500");
            baseResponse.setMessage("Create products un success");
            baseResponse.setMessageKh("Create products un success");
            return new ResponseEntity<>(baseResponse, HttpStatus.OK);
        }
    }

    @GetMapping("/api/products/{id}")
    public ResponseEntity<Object> getById(@PathVariable("id") int id){
        log.info("Intercept get product by id {}", id);
        baseResponse = new BaseResponse();
        try {
            var product = productService.getProductById(id);
            baseResponse.setCode("200");
            baseResponse.setMessage("Get product by id success");
            baseResponse.setMessageKh("Get product by id success");
            baseResponse.setData(product);
            return new ResponseEntity<>(baseResponse, HttpStatus.OK);
        }catch (Throwable e){
            log.info("While get error get product by id ", e);
            baseResponse.setCode("500");
            baseResponse.setMessage("Get product by id un success");
            baseResponse.setMessageKh("Get product by id un success");
            return new ResponseEntity<>(baseResponse, HttpStatus.OK);
        }
    }

    @PostMapping("/api/products/update")
    public ResponseEntity<Object> update(@RequestBody Product req){
        log.info("Intercept update product");
        baseResponse = new BaseResponse();
        try {
            log.info("Intercept update product req {}", req);
            productService.update(req);
            baseResponse.setCode("200");
            baseResponse.setMessage("Update products success");
            baseResponse.setMessageKh("Update products success");
            return new ResponseEntity<>(baseResponse, HttpStatus.OK);
        }catch (WebException e){
            log.error("Error update product on web exception ", e);
            baseResponse.setMessage(e.getMessage());
            baseResponse.setCode(e.getCode());
            baseResponse.setMessageKh(e.getMessageKh());
            return new ResponseEntity<>(baseResponse, HttpStatus.OK);
        }
        catch (Throwable e){
            log.info("While get error update products ", e);
            baseResponse.setCode("500");
            baseResponse.setMessage("Update products un success");
            baseResponse.setMessageKh("Update products un success");
            return new ResponseEntity<>(baseResponse, HttpStatus.OK);
        }
    }

    @PostMapping("/api/products/delete")
    public ResponseEntity<Object> delete(@RequestBody Product req){
        log.info("Intercept delete product");
        baseResponse = new BaseResponse();
        try {
            log.info("Intercept delete product req {}", req);
            productService.delete(req);
            baseResponse.setCode("200");
            baseResponse.setMessage("Delete products success");
            baseResponse.setMessageKh("Delete products success");
            return new ResponseEntity<>(baseResponse, HttpStatus.OK);
        }catch (WebException e){
            log.error("Error Delete product on web exception ", e);
            baseResponse.setMessage(e.getMessage());
            baseResponse.setCode(e.getCode());
            baseResponse.setMessageKh(e.getMessageKh());
            return new ResponseEntity<>(baseResponse, HttpStatus.OK);
        }
        catch (Throwable e){
            log.info("While get error Delete products ", e);
            baseResponse.setCode("500");
            baseResponse.setMessage("Delete products un success");
            baseResponse.setMessageKh("Delete products un success");
            return new ResponseEntity<>(baseResponse, HttpStatus.OK);
        }
    }

    @GetMapping("/api/products/units/{productId}")
    public ResponseEntity<Object> getAllProductUnit(@PathVariable("productId") Integer productId){
        log.info("Intercept get all product unit by product id {}", productId);
        baseResponse = new BaseResponse();
        try {
            var list = productService.getAllProductUnit(productId);
            baseResponse.setCode("200");
            baseResponse.setMessage("Get all products units success");
            baseResponse.setMessageKh("Get all products units success");
            baseResponse.setData(list);
            return new ResponseEntity<>(baseResponse, HttpStatus.OK);
        }catch (Throwable e){
            log.info("While get error get all products  units ", e);
            baseResponse.setCode("500");
            baseResponse.setMessage("Get all products unit un success");
            baseResponse.setMessageKh("Get all products unit un success");
            return new ResponseEntity<>(baseResponse, HttpStatus.OK);
        }
    }

    @PostMapping("/api/products/units/create")
    public ResponseEntity<Object> createProductUnit(@RequestBody ProductUnit req){
        log.info("Intercept create product unit {}", req);
        baseResponse = new BaseResponse();
        try {
            log.info("Intercept create product unit req {}", req);
            productService.createProductUnit(req);
            baseResponse.setCode("200");
            baseResponse.setMessage("Create products unit success");
            baseResponse.setMessageKh("Create products unit success");
            return new ResponseEntity<>(baseResponse, HttpStatus.OK);
        }catch (WebException e){
            log.error("Error create product unit on web exception ", e);
            baseResponse.setMessage(e.getMessage());
            baseResponse.setCode(e.getCode());
            baseResponse.setMessageKh(e.getMessageKh());
            return new ResponseEntity<>(baseResponse, HttpStatus.OK);
        }
        catch (Throwable e){
            log.info("While get error create products units ", e);
            baseResponse.setCode("500");
            baseResponse.setMessage("Create products unit un success");
            baseResponse.setMessageKh("Create products unit un success");
            return new ResponseEntity<>(baseResponse, HttpStatus.OK);
        }
    }

    @PostMapping("/api/products/units/update")
    public ResponseEntity<Object> updateProductUnit(@RequestBody ProductUnit req){
        log.info("Intercept update product unit {}", req);
        baseResponse = new BaseResponse();
        try {
            log.info("Intercept update product unit req {}", req);
            productService.updateProductUnit(req);
            baseResponse.setCode("200");
            baseResponse.setMessage("Update products unit success");
            baseResponse.setMessageKh("Update products unit success");
            return new ResponseEntity<>(baseResponse, HttpStatus.OK);
        }catch (WebException e){
            log.error("Error update product unit on web exception ", e);
            baseResponse.setMessage(e.getMessage());
            baseResponse.setCode(e.getCode());
            baseResponse.setMessageKh(e.getMessageKh());
            return new ResponseEntity<>(baseResponse, HttpStatus.OK);
        }
        catch (Throwable e){
            log.info("While get error update products units ", e);
            baseResponse.setCode("500");
            baseResponse.setMessage("update products unit un success");
            baseResponse.setMessageKh("update products unit un success");
            return new ResponseEntity<>(baseResponse, HttpStatus.OK);
        }
    }

    @PostMapping("/api/products/units/delete")
    public ResponseEntity<Object> deleteProductUnit(@RequestBody ProductUnit req){
        log.info("Intercept delete product unit {}", req);
        baseResponse = new BaseResponse();
        try {
            log.info("Intercept delete product unit req {}", req);
            productService.deleteProductUnit(req);
            baseResponse.setCode("200");
            baseResponse.setMessage("delete products unit success");
            baseResponse.setMessageKh("delete products unit success");
            return new ResponseEntity<>(baseResponse, HttpStatus.OK);
        }catch (WebException e){
            log.error("Error delete product unit on web exception ", e);
            baseResponse.setMessage(e.getMessage());
            baseResponse.setCode(e.getCode());
            baseResponse.setMessageKh(e.getMessageKh());
            return new ResponseEntity<>(baseResponse, HttpStatus.OK);
        }
        catch (Throwable e){
            log.info("While get error delete products units ", e);
            baseResponse.setCode("500");
            baseResponse.setMessage("delete products unit un success");
            baseResponse.setMessageKh("delete products unit un success");
            return new ResponseEntity<>(baseResponse, HttpStatus.OK);
        }
    }


    @PostMapping("/api/products/scan")
    public ResponseEntity<Object> scanProduct(@RequestBody ProductRequest req){
        log.info("Intercept delete product unit {}", req);
        baseResponse = new BaseResponse();
        try {
            log.info("Intercept scan product unit req {}", req);
            baseResponse.setData(productService.searchProduct(req));
            return new ResponseEntity<>(baseResponse, HttpStatus.OK);
        }catch (WebException e){
            log.error("Error scan product unit  web exception ", e);
            baseResponse.setMessage(e.getMessage());
            baseResponse.setCode(e.getCode());
            baseResponse.setMessageKh(e.getMessageKh());
            return new ResponseEntity<>(baseResponse, HttpStatus.OK);
        }
        catch (Throwable e){
            log.info("While get error scan products  ", e);
            baseResponse.setCode("500");
            baseResponse.setMessage("scan products  un success");
            baseResponse.setMessageKh("scan products unit un success");
            return new ResponseEntity<>(baseResponse, HttpStatus.OK);
        }
    }


}
