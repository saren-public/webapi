package com.dinsaren.webapi.controllers;

import com.dinsaren.webapi.exceptions.WebException;
import com.dinsaren.webapi.models.Category;
import com.dinsaren.webapi.models.response.BaseResponse;
import com.dinsaren.webapi.services.CategoryService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;


@Controller
@RequiredArgsConstructor
@Slf4j
public class CategoryController {
    private final CategoryService categoryService;
    private BaseResponse baseResponse;

    @GetMapping("/api/categories")
    public ResponseEntity<Object> getAllCategory(){
        log.info("Intercept get all categories");
        baseResponse = new BaseResponse();
        try {
            var list = categoryService.getAllCategories();
            baseResponse.setCode("200");
            baseResponse.setMessage("Get all categories success");
            baseResponse.setMessageKh("Get all categories success");
            baseResponse.setData(list);
            return new ResponseEntity<>(baseResponse, HttpStatus.OK);
        }catch (Throwable e){
            log.info("While get error get all categories ", e);
            baseResponse.setCode("500");
            baseResponse.setMessage("Get all categories un success");
            baseResponse.setMessageKh("Get all categories un success");
            return new ResponseEntity<>(baseResponse, HttpStatus.OK);
        }
    }

    @GetMapping("/api/categories/{id}")
    public ResponseEntity<Object> getById(@PathVariable("id") Integer id){
        log.info("Intercept get category id {}", id);
        baseResponse = new BaseResponse();
        try {
            var category = categoryService.getCategoryById(id);
            baseResponse.setCode("200");
            baseResponse.setMessage("Get category by id success");
            baseResponse.setMessageKh("Get category by id success");
            baseResponse.setData(category);
            return new ResponseEntity<>(baseResponse, HttpStatus.OK);
        }catch (Throwable e){
            log.info("While get error get category by id ", e);
            baseResponse.setCode("500");
            baseResponse.setMessage("Get category by id un success");
            baseResponse.setMessageKh("Get category by id success un success");
            return new ResponseEntity<>(baseResponse, HttpStatus.OK);
        }
    }

    @PostMapping("/api/categories/create")
    public ResponseEntity<Object> createCategory(@RequestBody Category req){
        baseResponse = new BaseResponse();
        try{
            log.info("Intercept create category req {}", req);
            categoryService.create(req);
            return new ResponseEntity<>(baseResponse, HttpStatus.OK);
        }catch (WebException e){
            baseResponse.setMessage(e.getMessage());
            baseResponse.setCode(e.getCode());
            baseResponse.setMessageKh(e.getMessageKh());
            return new ResponseEntity<>(baseResponse, HttpStatus.OK);
        }
        catch (Throwable e){
            return new ResponseEntity<>(baseResponse, HttpStatus.OK);
        }
    }

    @PostMapping("/api/categories/update")
    public ResponseEntity<Object> updateCategory(@RequestBody Category req){
        try{
            log.info("Intercept create category req {}", req);
            categoryService.update(req);
            return new ResponseEntity<>(baseResponse, HttpStatus.OK);
        }catch (Throwable e){
            return new ResponseEntity<>(baseResponse, HttpStatus.OK);
        }
    }


}
