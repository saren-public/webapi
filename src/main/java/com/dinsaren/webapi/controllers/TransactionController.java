package com.dinsaren.webapi.controllers;

import com.dinsaren.webapi.exceptions.WebException;
import com.dinsaren.webapi.models.Product;
import com.dinsaren.webapi.models.Transaction;
import com.dinsaren.webapi.models.response.BaseResponse;
import com.dinsaren.webapi.services.TransactionService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@Slf4j
public class TransactionController {
    private final TransactionService transactionService;
    private BaseResponse baseResponse;

    @PostMapping("/api/transactions/create")
    public ResponseEntity<Object> create(@RequestBody Transaction req){
        log.info("Intercept create category");
        baseResponse = new BaseResponse();
        try {
            log.info("Intercept create Transaction req {}", req);
            transactionService.createTransaction(req);
            baseResponse.setCode("200");
            baseResponse.setMessage("Create Transaction success");
            baseResponse.setMessageKh("Create Transaction success");
            return new ResponseEntity<>(baseResponse, HttpStatus.OK);
        }catch (WebException e){
            log.error("Error create Transaction on web exception ", e);
            baseResponse.setMessage(e.getMessage());
            baseResponse.setCode(e.getCode());
            baseResponse.setMessageKh(e.getMessageKh());
            return new ResponseEntity<>(baseResponse, HttpStatus.OK);
        }
        catch (Throwable e){
            log.info("While get error Transaction products ", e);
            baseResponse.setCode("500");
            baseResponse.setMessage("Create Transaction un success");
            baseResponse.setMessageKh("Create Transaction un success");
            return new ResponseEntity<>(baseResponse, HttpStatus.OK);
        }
    }

}
