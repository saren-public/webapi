package com.dinsaren.webapi.constants;

public interface Constants {
    String STATUS_ACTIVE = "ACT";
    String STATUS_DELETE = "DEL";
    String STATUS_DISABLE = "DIS";
    String Y = "Y";
    String N = "N";
}
