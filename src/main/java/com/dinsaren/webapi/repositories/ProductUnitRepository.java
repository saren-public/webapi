package com.dinsaren.webapi.repositories;

import com.dinsaren.webapi.models.ProductUnit;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ProductUnitRepository extends JpaRepository<ProductUnit,Integer> {
    List<ProductUnit> findAllByProduct_IdOrderBySaleDefaultDesc(Integer productId);
    ProductUnit findByProduct_IdAndUnitType_Id(Integer productId, Integer unitTypeId);
    ProductUnit findBySaleDefaultAndProductId(String saleDefault, Integer productId);


}
