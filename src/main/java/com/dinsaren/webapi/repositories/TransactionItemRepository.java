package com.dinsaren.webapi.repositories;

import com.dinsaren.webapi.models.Transaction;
import com.dinsaren.webapi.models.TransactionItem;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TransactionItemRepository extends JpaRepository<TransactionItem, Integer> {

}
