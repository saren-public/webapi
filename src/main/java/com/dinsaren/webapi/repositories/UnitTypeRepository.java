package com.dinsaren.webapi.repositories;

import com.dinsaren.webapi.models.UnitType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UnitTypeRepository extends JpaRepository<UnitType, Integer> {
    List<UnitType> findAllByStatus(String status);
    Optional<UnitType> findByName(String name);
}
