package com.dinsaren.webapi.repositories;

import com.dinsaren.webapi.models.Product;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ProductRepository extends JpaRepository<Product, Integer> {
    Optional<Product> findByCode(String code);
    Optional<Product> findByBarcode(String barcode);
    Page<Product> findAllByStatusOrderByIdDesc(String status, Pageable pageable);
}
