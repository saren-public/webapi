package com.dinsaren.webapi.repositories;

import com.dinsaren.webapi.models.ERole;
import com.dinsaren.webapi.models.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface RoleRepository extends JpaRepository<Role, Long> {
  Optional<Role> findByName(ERole name);
}
