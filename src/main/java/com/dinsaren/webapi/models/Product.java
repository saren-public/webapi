package com.dinsaren.webapi.models;

import jakarta.persistence.*;
import lombok.*;

import java.util.List;

@Entity
@Table(name = "products")
@Getter
@Setter
@ToString
@AllArgsConstructor // Constructor have parameter
@NoArgsConstructor // Default Constructor
public class Product extends BaseEntity{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @ManyToOne
    private Category category;
    private String name;
    private String code;
    private String barcode;
    private String description;
    private double qtyOnHand;
    @Column(length = 1)
    private String stockType;
    @Column(length = 3)
    private String status;
    @Transient
    private List<ProductUnit> productUnits;
}
