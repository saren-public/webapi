package com.dinsaren.webapi.models.response;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ProductRequest {
    private String code;
    private String barcode;
}
