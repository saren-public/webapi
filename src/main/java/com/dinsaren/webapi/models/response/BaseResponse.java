package com.dinsaren.webapi.models.response;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BaseResponse {
    private String code;
    private String message;
    private String messageKh;
    private Object data;


    public void setGetDataSuccess(Object data){
        this.code = "200";
        this.message = "Get All Data Success";
        this.messageKh = "Get All Data Success";
        this.data = data;
    }
    public void setCreateDataSuccess(Object data){
        this.code = "200";
        this.message = "Create Data Success";
        this.messageKh = "Create Data Success";
        this.data = data;
    }
    public void setUpdateDataSuccess(Object data){
        this.code = "200";
        this.message = "Update Data Success";
        this.messageKh = "Update Data Success";
        this.data = data;
    }
    public void setDeleteDataSuccess(Object data){
        this.code = "200";
        this.message = "Delete Data Success";
        this.messageKh = "Delete Data Success";
        this.data = data;
    }
}
