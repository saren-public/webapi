package com.dinsaren.webapi.models;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.Date;

@Table(name = "transaction_items")
@Entity
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class TransactionItem {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private int transactionId;
    private int productId;
    private int unitTypeId;
    private BigDecimal cost;
    private BigDecimal price;
    private BigDecimal discount;

}
