package com.dinsaren.webapi.models;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@Table(name = "transactions")
@Entity
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class Transaction {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String invoiceNo;
    private Date transactionDate;
    private int userId;
    private int customerId;
    private BigDecimal discount;
    private BigDecimal amount;
    private String status;
    @Transient
    public List<TransactionItem> items;
}
