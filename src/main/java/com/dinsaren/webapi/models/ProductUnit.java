package com.dinsaren.webapi.models;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "product_units")
@Setter
@Getter
public class ProductUnit {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @ManyToOne
//    @JoinColumn(name = "product_id",  nullable = false)
    private Product product;
    @ManyToOne
//    @JoinColumn(name = "product_id", nullable = false)
    private UnitType unitType;
    private Double price;
    private Double cost;
    private String saleDefault;
}
